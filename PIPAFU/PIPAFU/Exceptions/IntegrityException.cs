﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIPAFU.Exceptions
{
    public class IntegrityException : ApplicationException //erros de integridade referencial: Quando apago um registro de uma tabela que tem FK de outra
    {
        public IntegrityException(string message) : base(message)
        {
        }
    }
}
