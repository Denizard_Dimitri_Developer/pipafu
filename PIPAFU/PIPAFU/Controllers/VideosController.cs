﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PIPAFU.Data;
using PIPAFU.Models;
using PIPAFU.Models.ViewModels;
using PIPAFU.Services;
using VimeoDotNet;
using VimeoDotNet.Net;

namespace PIPAFU.Controllers
{
    public class VideosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly VimeoApiService _vimeoApiService;

        public VideosController(ApplicationDbContext context, VimeoApiService vimeoApiService)
        {
            _context = context;
            _vimeoApiService = vimeoApiService;
        }

        // GET: Videos
        public async Task<IActionResult> Index(int? cursoId, bool? editar = false)
        {
            ViewData["Editar"] = editar;
            var videos = await _context.Video.Where(v => v.Curso.Id == cursoId).ToListAsync();
            ViewData["cursoId"] = cursoId;
            ViewData["nomeCurso"] = _context.Curso.Where(c => c.Id == cursoId).Select(c => c.Nome).FirstOrDefault();
            return View(videos);
        }

        public async Task<IActionResult> Video(int? clipId, int cursoId)
        {
            var videoEscolhido = await _context.Video.Where(v => v.ClipId == clipId.Value).FirstOrDefaultAsync();
            var videos = await _context.Video.Where(v => v.Curso.Id == cursoId).ToListAsync();
            var curso = await _context.Curso.Where(c => c.Id == cursoId).FirstOrDefaultAsync();

            var videoViewModel = new VideoViewModel { Video = videoEscolhido, Videos = videos, Curso = curso };

            return View(videoViewModel);
        }

        // GET: Videos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var video = await _context.Video
                .FirstOrDefaultAsync(m => m.Id == id);
            if (video == null)
            {
                return NotFound();
            }

            return View(video);
        }

        // GET: Videos/Create
        //[DisableRequestSizeLimit]
        [DisableRequestSizeLimit, RequestFormLimits(MultipartBodyLengthLimit = Int32.MaxValue, ValueLengthLimit = Int32.MaxValue)]
        public async Task<IActionResult> Create(int? cursoId)
        {
            var curso = await _context.Curso.Where(c => c.Id == cursoId.Value).FirstOrDefaultAsync();
            var viewModel = new VideoViewModel { Curso = curso };
            return View(viewModel);
        }

        // POST: Videos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[DisableRequestSizeLimit]
        [DisableRequestSizeLimit, RequestFormLimits(MultipartBodyLengthLimit = Int32.MaxValue, ValueLengthLimit = Int32.MaxValue)]
        public async Task<IActionResult> Create(Video video)
        {
            var files = Request.Form.Files;
            IFormFile file = files[0];

            if (ModelState.IsValid)
            {
                video = await _vimeoApiService.UploadVimeoAsync(file, video);
                video.Data = DateTime.Now;
                _context.Add(video);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { cursoId = video.CursoId, editar = true });
            }
            return View(video);
        }

        // GET: Videos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var video = await _context.Video.FindAsync(id);
            if (video == null)
            {
                return NotFound();
            }
            return View(video);
        }

        // POST: Videos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ChunkSize,BytesWritten,IsVerifiedComplete,ClipUri,FileLength,ClipId")] Video video)
        {
            if (id != video.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(video);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VideoExists(video.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(video);
        }

        // GET: Videos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var video = await _context.Video
                .FirstOrDefaultAsync(m => m.Id == id);
            if (video == null)
            {
                return NotFound();
            }

            return View(video);
        }

        // POST: Videos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var video = await _context.Video.FindAsync(id);
            await _vimeoApiService.DeleteVideo(video.ClipId);
            _context.Video.Remove(video);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VideoExists(int id)
        {
            return _context.Video.Any(e => e.Id == id);
        }
    }
}
