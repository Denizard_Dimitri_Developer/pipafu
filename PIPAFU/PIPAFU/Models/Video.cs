﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIPAFU.Models
{
    public class Video
    {
        public int Id { get; set; }
        public Curso Curso { get; set; }
        public int CursoId { get; set; }
        public string Nome { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name ="Descrição")]
        public string Descricao { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Data { get; set; }
        public long? ClipId { get; set; }
        public string ClipUri { get; set; }
        public long FileLength { get; set; }
        public int ChunkSize { get; set; }
        public long BytesWritten { get; set; }
        public bool IsVerifiedComplete { get; set; }

        public Video()
        {
        }

        public Video(int chunkSize, long bytesWritten, bool isVerifiedComplete, string clipUri, long fileLength, long? clipId, Curso curso)
        {
            ChunkSize = chunkSize;
            BytesWritten = bytesWritten;
            IsVerifiedComplete = isVerifiedComplete;
            ClipUri = clipUri;
            FileLength = fileLength;
            ClipId = clipId;
            Curso = curso;
        }
    }
}
