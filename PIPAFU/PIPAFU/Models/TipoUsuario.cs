﻿namespace PIPAFU.Models
{
    public class TipoUsuario
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}