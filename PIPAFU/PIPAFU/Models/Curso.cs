﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIPAFU.Models
{
    public class Curso
    {
        public int Id { get; set; }
        [Display(Name = "Nome do Curso")]
        public string Nome { get; set; }
        [DisplayFormat(/*ApplyFormatInEditMode = true, */DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Data { get; set; }
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        //public ICollection<ApplicationUser> Alunos { get; set; } = new List<ApplicationUser>(); //para a referência de Curso em ApplicationUser
        public ApplicationUser Professor { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ApplicationUserId { get; set; } //ProfessorFK
        public ICollection<Video> Videos { get; set; } = new List<Video>();

        public Curso()
        {
        }

        public Curso(string nome, DateTime data, string descricao, ApplicationUser professor)
        {
            Nome = nome;
            Data = data;
            Descricao = descricao;
            Professor = professor;
        }

        public void AddVideo(Video video)
        {
            Videos.Add(video);
        }

        public void RemoveVideo(Video video)
        {
            Videos.Remove(video);
        }

        public List<Video> GetVideos()
        {
            return Videos.ToList();
        }
    }
}
