﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIPAFU.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [Display(Name = "Nome de Usuário")]
        public string NomeUsuario { get; set; }
        public ICollection<Curso> Cursos { get; set; } = new List<Curso>();
        //public Curso Curso { get; set; } //para a lista de alunos em Curso
        //public int CursoId { get; set; } //para a lista de alunos em Curso
        public virtual TipoUsuario TipoUsuario { get; set; }
        public int TipoUsuarioId { get; set; }

        public void AddCurso(Curso curso)
        {
            Cursos.Add(curso);
        }

        public List<Curso> GetCursos()
        {
            return Cursos.ToList();
        }
    }
}
