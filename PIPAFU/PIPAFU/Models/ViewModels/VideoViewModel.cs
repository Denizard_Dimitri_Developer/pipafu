﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIPAFU.Models.ViewModels
{
    public class VideoViewModel
    {
        public Video Video { get; set; }
        public ICollection<Video> Videos { get; set; }
        public Curso Curso { get; set; }
    }
}
