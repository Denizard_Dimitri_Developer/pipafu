﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PIPAFU.Data;
using PIPAFU.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VimeoDotNet;
using VimeoDotNet.Net;

namespace PIPAFU.Services
{
    public class VimeoApiService 
    {
        //string accessToken = "8e631984a554c671698d0f2a49cf2edc"; //desenv
        string accessToken = "f4fd0c552692c994b6edcff93c035f75"; //PROD

        public async Task<Video> UploadVimeoAsync(IFormFile file, Video video)
        {
            string uploadstatus = "";
            try
            {
                if (file != null)
                {
                    VimeoClient vimeoClient = new VimeoClient(accessToken);
                    var authcheck = await vimeoClient.GetAccountInformationAsync();

                    if (authcheck.Name != null)
                    {
                        IUploadRequest uploadRequest = new UploadRequest();
                        BinaryContent binaryContent = new BinaryContent(file.OpenReadStream(), file.ContentType);

                        long chunksizeLong = 0;
                        long contentleght = file.Length;
                        long temp1 = contentleght / 1024;
                        if (temp1 > 1)
                        {
                            chunksizeLong = temp1 / 1024;
                            chunksizeLong = chunksizeLong / 10;
                            chunksizeLong = chunksizeLong * 1048576;
                        }
                        else
                        {
                            chunksizeLong = 1048576;
                        }

                        int chunksize = unchecked((int)chunksizeLong);
                        uploadRequest = await vimeoClient.UploadEntireFileAsync(binaryContent, chunksize, null); //, chunksize
                        uploadstatus = String.Concat("file uploaded ", "https://vimeo.com/", uploadRequest.ClipId.Value.ToString(), "/none");

                        video.ClipId = uploadRequest.ClipId;
                        video.ClipUri = uploadRequest.ClipUri;
                        video.FileLength = uploadRequest.FileLength;
                        video.BytesWritten = uploadRequest.BytesWritten;
                        video.ChunkSize = uploadRequest.ChunkSize;
                        video.IsVerifiedComplete = uploadRequest.IsVerifiedComplete;
                    }
                }
                return video;
            }
            catch (Exception e)
            {
                uploadstatus = "not uploaded: " + e.Message;
                if (e.InnerException != null)
                {
                    uploadstatus += e.InnerException.Message;
                }
                return video;
            }
        }

        public async Task DeleteVideo(long? clipId)
        {
            try
            {
                if (clipId > 0)
                {
                    VimeoClient vimeoClient = new VimeoClient(accessToken);
                    await vimeoClient.DeleteVideoAsync(clipId.Value);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        //public async Task AuthenticateAsync()
        //{
        //    try
        //    {
        //        VimeoClient vimeoClient = new VimeoClient(accessToken);
        //        var authcheck = await vimeoClient.GetAccountInformationAsync();
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("ERRO: " + e.Message);
        //    }
        //}
    }
}
