﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIPAFU.Migrations
{
    public partial class VideoProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Descricao",
                table: "Video",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nome",
                table: "Video",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Descricao",
                table: "Video");

            migrationBuilder.DropColumn(
                name: "Nome",
                table: "Video");
        }
    }
}
