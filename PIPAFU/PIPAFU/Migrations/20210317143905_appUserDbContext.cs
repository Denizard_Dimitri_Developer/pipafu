﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIPAFU.Migrations
{
    public partial class appUserDbContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Curso_AspNetUsers_ApplicationUserId",
                table: "Curso");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Curso",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserIde",
                table: "Curso",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Curso_AspNetUsers_ApplicationUserId",
                table: "Curso",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Curso_AspNetUsers_ApplicationUserId",
                table: "Curso");

            migrationBuilder.DropColumn(
                name: "ApplicationUserIde",
                table: "Curso");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Curso",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Curso_AspNetUsers_ApplicationUserId",
                table: "Curso",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
