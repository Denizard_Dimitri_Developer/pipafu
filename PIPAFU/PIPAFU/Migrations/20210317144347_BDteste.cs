﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIPAFU.Migrations
{
    public partial class BDteste : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Nomes",
                table: "Curso",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nomes",
                table: "Curso");
        }
    }
}
