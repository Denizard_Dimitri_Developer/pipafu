﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIPAFU.Migrations
{
    public partial class CursoFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Curso_AspNetUsers_ProfessorId",
                table: "Curso");

            migrationBuilder.DropIndex(
                name: "IX_Curso_ProfessorId",
                table: "Curso");

            migrationBuilder.DropColumn(
                name: "ProfessorId",
                table: "Curso");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "Curso",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Curso_ApplicationUserId",
                table: "Curso",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Curso_AspNetUsers_ApplicationUserId",
                table: "Curso",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Curso_AspNetUsers_ApplicationUserId",
                table: "Curso");

            migrationBuilder.DropIndex(
                name: "IX_Curso_ApplicationUserId",
                table: "Curso");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Curso");

            migrationBuilder.AddColumn<string>(
                name: "ProfessorId",
                table: "Curso",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Curso_ProfessorId",
                table: "Curso",
                column: "ProfessorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Curso_AspNetUsers_ProfessorId",
                table: "Curso",
                column: "ProfessorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
