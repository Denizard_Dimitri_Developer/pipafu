﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIPAFU.Migrations
{
    public partial class cursoIDemVideo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Video_Curso_CursoId",
                table: "Video");

            migrationBuilder.AlterColumn<int>(
                name: "CursoId",
                table: "Video",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Video_Curso_CursoId",
                table: "Video",
                column: "CursoId",
                principalTable: "Curso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Video_Curso_CursoId",
                table: "Video");

            migrationBuilder.AlterColumn<int>(
                name: "CursoId",
                table: "Video",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Video_Curso_CursoId",
                table: "Video",
                column: "CursoId",
                principalTable: "Curso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
