﻿using PIPAFU.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIPAFU.Data
{
    public class SeedingService
    {
        private ApplicationDbContext _context;
        public SeedingService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            if (_context.ApplicationUser.Any() || _context.Curso.Any() || _context.Video.Any() || _context.TipoUsuario.Any())
            {
                return; //BD já foi populado
            }
            //return;
            TipoUsuario professor = new TipoUsuario { /*Id = 1,*/ Tipo = "Professor" };
            TipoUsuario aluno = new TipoUsuario { /*Id = 2,*/ Tipo = "Aluno" };

            ApplicationUser aluno1 = new ApplicationUser
            { 
                Id = "734e4727-23e2-4117-ad3f-bc36e6907b02",
                UserName = "denizarddimitri.developer@gmail.com",
                NormalizedUserName = "DENIZARDDIMITRI.DEVELOPER@GMAIL.COM",
                Email = "denizarddimitri.developer@gmail.com",
                NormalizedEmail = "DENIZARDDIMITRI.DEVELOPER@GMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAEGipJ8/1zzlYnBoGQFQSzrxtJnp4EcsX6tTv2qfaiN1zBAbPFuwK+02PceuJSAq3EQ==",
                SecurityStamp = "QBTGEUSG6MEDQUL3JWX7UCCPBDUYH7HJ",
                ConcurrencyStamp = "64f74ad3-06ba-4bdd-aab9-81948c12769c",
                PhoneNumber = null,
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnd = null,
                LockoutEnabled = true,
                AccessFailedCount = 0,
                NomeUsuario = "Dimitri Developer",
                TipoUsuario = aluno
            };
            ApplicationUser aluno2 = new ApplicationUser
            {
                Id = "4bf28c69-9fed-44e5-b591-95113e6e4d36",
                UserName = "denizarddimitricamargo@gmail.com",
                NormalizedUserName = "DENIZARDDIMITRICAMARGO@GMAIL.COM",
                Email = "denizarddimitricamargo@gmail.com",
                NormalizedEmail = "DENIZARDDIMITRICAMARGO@GMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAEEWj0wHcLDd5ZoSlZWY7QhJFi/sdKch9QMe2iSWYYjR0FaUxNczL8IRQzHMpsavfKA==",
                SecurityStamp = "ZXMXRNELHYDX3TBI4GX7AFFIS5OKARLA",
                ConcurrencyStamp = "5a68fbc7-1737-4384-905d-2164963c37b8",
                PhoneNumber = null,
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnd = null,
                LockoutEnabled = true,
                AccessFailedCount = 0,
                NomeUsuario = "Denizard Dimitri Camargo",
                TipoUsuario = aluno
            };
            ApplicationUser professor1 = new ApplicationUser
            {
                Id = "2845173f-75a6-4baa-ad12-8ca8fbbfbc1d",
                UserName = "giovan.marco@gmail.com",
                NormalizedUserName = "GIOVAN.MARCO@GMAIL.COM",
                Email = "giovan.marco@gmail.com",
                NormalizedEmail = "GIOVAN.MARCO@GMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAEHa8bp4TokT7/PDR6ZkG5rmxv4t9cLncQH+g5ElDf2V/lhHY+Kf7KA9kNaGlM6TcdQ==",
                SecurityStamp = "BEONLLRY6XMYEML5PANDKSQP37MK3Z2E",
                ConcurrencyStamp = "25f540bc-c316-4987-a9e0-23794e846859",
                PhoneNumber = null,
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnd = null,
                LockoutEnabled = true,
                AccessFailedCount = 0,
                NomeUsuario = "Giovan Marco",
                TipoUsuario = professor
            };
            ApplicationUser professor2 = new ApplicationUser
            {
                Id = "25a2aeed-c830-4004-a9b4-8f0c8934f035",
                UserName = "jorge.maia@gmail",
                NormalizedUserName = "JORGE.MAIA@GMAIL",
                Email = "jorge.maia@gmail",
                NormalizedEmail = "JORGE.MAIA@GMAIL",
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAEEK/LoYcl4DMTJ3Ovz7rg/KUnz/zICTjHfRzht4Ayz1g5XUmA5IWl4vK4NkDTJQRZQ==",
                SecurityStamp = "GKTBEQ7Y75MQ2KFO6ZNRTZVR2FSF4WY2",
                ConcurrencyStamp = "01aed189-1201-41ea-b375-e212d557d5b8",
                PhoneNumber = null,
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnd = null,
                LockoutEnabled = true,
                AccessFailedCount = 0,
                NomeUsuario = "Jorge Maia",
                TipoUsuario = professor
            };
            ApplicationUser professor3 = new ApplicationUser
            {
                Id = "e808f092-a242-42ca-bc28-ffa1211325c2",
                UserName = "liluyoud.lacerda@gmail.com",
                NormalizedUserName = "LILUYOUD.LACERDA@GMAIL.COM",
                Email = "liluyoud.lacerda@gmail.com",
                NormalizedEmail = "LILUYOUD.LACERDA@GMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAEKIgIG2BL/tTWLPq0O0I9JD8N/uc4pr7nPAXZkC3vjkG0CFrCmpYcT24XVN5cwgixw==",
                SecurityStamp = "NWTML56MGQTA6RR2DFYQSBHZPEAVRKGU",
                ConcurrencyStamp = "7c7b459c-e78e-4611-8ee1-26fab2dd51d4",
                PhoneNumber = null,
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnd = null,
                LockoutEnabled = true,
                AccessFailedCount = 0,
                NomeUsuario = "Lilo Lacerda",
                TipoUsuario = professor
            };
            Curso curso1 = new Curso
            {
                //Id = 1,
                Nome = "Eu sei que você sabe programar",
                Data = new DateTime(DateTime.Now.Month),
                Descricao = "Lab. de Programação 1",
                Professor = professor1, //giovan
            };
            Curso curso2 = new Curso
            {
                //Id = 2,
                Nome = "Programando Orientado a Objetos Como um Profissional",
                Data = new DateTime(DateTime.Now.Month),
                Descricao = "Programação Orientada a Objetos",
                Professor = professor1, //giovan
            };
            Curso curso3 = new Curso
            {
                //Id = 3,
                Nome = "Introdução ao Mobile",
                Data = new DateTime(DateTime.Now.Month),
                Descricao = "Android Studio com Java",
                Professor = professor1, //giovan
            };
            Curso curso4 = new Curso
            {
                //Id = 4,
                Nome = "Do Graveto ao Código Binário",
                Data = new DateTime(DateTime.Now.Month),
                Descricao = "Lógica Computacional",
                Professor = professor2, //jorge
            };
            Video v1 = new Video
            {
                //Id = 1,
                Curso = curso1,
                Nome = "Aula 1",
                Descricao = "1° Video teste do Curso: Eu sei que você sabe programar",
                ClipId = 1,
                ClipUri = "/ClipUri",
                FileLength = 1,
                ChunkSize = 1,
                BytesWritten = 1,
                IsVerifiedComplete = true
            };
            Video v2 = new Video
            {
                //Id = 2,
                Curso = curso1,
                Nome = "Aula 2",
                Descricao = "2° Video teste do Curso: Eu sei que você sabe programar",
                ClipId = 1,
                ClipUri = "/ClipUri",
                FileLength = 1,
                ChunkSize = 1,
                BytesWritten = 1,
                IsVerifiedComplete = true
            };
            Video v3 = new Video
            {
                //Id = 3,
                Curso = curso1,
                Nome = "Aula 3",
                Descricao = "3° Video teste do Curso: Eu sei que você sabe programar",
                ClipId = 1,
                ClipUri = "/ClipUri",
                FileLength = 1,
                ChunkSize = 1,
                BytesWritten = 1,
                IsVerifiedComplete = true
            };
            Video v4 = new Video
            {
                //Id = 4,
                Curso = curso2,
                Nome = "Aula 1",
                Descricao = "Video teste do Curso: Programando Orientado a Objetos Como um Profissional",
                ClipId = 1,
                ClipUri = "/ClipUri",
                FileLength = 1,
                ChunkSize = 1,
                BytesWritten = 1,
                IsVerifiedComplete = true
            };

            _context.TipoUsuario.AddRange(professor, aluno);
            _context.ApplicationUser.AddRange(aluno1, aluno2, professor1, professor2, professor3);
            _context.Curso.AddRange(curso1, curso2, curso3, curso4);
            _context.Video.AddRange(v1, v2, v3, v4);
            _context.SaveChanges();
        }
    }
}
