﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PIPAFU.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PIPAFU.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<ApplicationUser> ApplicationUser { get; set; }
        public DbSet<Video> Video { get; set; }
        public DbSet<Curso> Curso { get; set; }
        public DbSet<TipoUsuario> TipoUsuario { get; set; }
    }
}
